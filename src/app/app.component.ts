import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'style-binding';

  obj = {
    type: 'carry-on',
    weight: 20,
    color: 'purple',
    destination: 'China'
  };


  /*
function myFun(p1, p2) {
  return p1 + p2;
  }



 test( ) {
  let span = 30;
  setTimeout(function myFn(function youFun(p3) {
    return p3;
    }, p2) {
    return p1 + p2;
  }, 1000);

  setTimeout((p3 => p3, p2) => {console.log(span); thisreturn p1 + p2; }, 1000)

     p3 => { console.log(p3); return p3;}
    (p1, p2) => {}
    () => console.log('hi baby')

    let newObj = {...this.obj, weight: 15 };
    let {weight, destination, ...reskeys} = this.obj;

    let weight = this.obj.weight;
    let weight = this.obj.weight;
    let reskeys = {'x': this.., 'des': dafds}
    console.log(weight, destination)
    console.log(reskeys)

  }
  */
}
