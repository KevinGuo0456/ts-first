import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ObjectUnsubscribedError, Observable} from "rxjs";
import {Passenger} from "../models/passenger.ts";
const HTTP_URL = 'http://localhost:3000/passengers';

@Injectable({
  providedIn: 'root'
})
export class PassengersService {
  private _id: number;

  constructor(private http: HttpClient) {
    console.log(this.http)
  }
  //Restful api
  getPassengers(): Observable<Object> {
    //console.log('service running')
    //RXJS
    return this.http.get(HTTP_URL);
  }
  getPassenger( id  : number): Observable<Object> {
    this._id = id;
    //return this.http.get(HTTP_URL + '/' + id)
    let str = `${HTTP_URL}/${id}`;
    console.log(str);
    return this.http.get(`${HTTP_URL}/${id}`)
    //es6 string
  }
   putPassenger(passenger: Passenger): Observable<Object> {
    console.log('get put passenger fine');
    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      'myname': 'Kevin888'
    });

    //RXJS
    // @ts-ignore
     return this.http.put(HTTP_URL + '/' + passenger.id, passenger, { headers: headers })
  }
  deletePassenger(passenger: Passenger) {
    console.log('delete passenger fine');
    return this.http.delete(HTTP_URL + '/' + passenger.id);
  }
}
