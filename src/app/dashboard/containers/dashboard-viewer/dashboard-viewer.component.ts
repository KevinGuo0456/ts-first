import { Component, OnInit } from '@angular/core';
import {PassengersService} from "../../services/passengers.service";
import {Passenger} from "../../models/passenger.ts";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-dashboard-viewer',
  templateUrl: './dashboard-viewer.component.html',
  styleUrls: ['./dashboard-viewer.component.css']
})
export class DashboardViewerComponent implements OnInit {

  passenger: Passenger = null;

  constructor(private  passengerService: PassengersService) { }

  ngOnInit() {
    this.passengerService.getPassenger(1).subscribe(obj => this.passenger = <Passenger>obj);
  }

}
