import {Component, OnInit} from '@angular/core';
import {Passenger} from "../../models/passenger.ts";
import {PassengersService} from "../../services/passengers.service";

const g_errorFirstapp = 'error_firstapp';
@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.scss']
})
export class DashComponent implements OnInit{
  errors: boolean;
  errorText: string;
  passengers: Passenger[];

  constructor(private passengerService: PassengersService) {
    this.passengerService.getPassengers().subscribe(obj=> this.passengers = <Passenger[]> obj);
    this.errors = false;
    this.errorText = '';
  }

  ngOnInit() {
    let errors = localStorage.getItem(g_errorFirstapp);
    if (errors){
      console.log(errors)
    }
  }

  handleRemove(event){
    console.log('receiving event:' ,event);
    this.passengerService.deletePassenger(event).subscribe(
      () => this.passengers = this.passengers.filter(p=> p.id !== event.id))
  }

  handleEdit(event: Passenger) {

    this.passengerService.putPassenger(event).subscribe((obj: Passenger) => {
      this.passengers = this.passengers.map(p => {
        if(p.id ===obj.id){
          p = {...p,...obj};
        }
        return p;
      })
    },
      error => { console.log('there is error', error)}
      );
    /*console.log(event);
    this.passengers = this.passengers.map(p => {
      console.log("p1 :" + stringify(p));
        if (p.id === event.id) {

          p = {...p, ...event};
        }
      return p
      }
    )*/
  }
}
