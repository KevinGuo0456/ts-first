import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Passenger} from "../../models/passenger.ts";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnChanges{
  editing : boolean= false;
  @Input() detail: Passenger;
  @Output() remove: EventEmitter<Passenger> = new EventEmitter();
  @Output() edit: EventEmitter<Passenger> = new EventEmitter();
  constructor() {

  }

  ngOnInit() {
  }

  ngOnChanges(change){
    console.log(change);
    if(change.detail){
      this.detail = {...change.detail.currentValue}
    }
  }
  toggleEdit(){
    this.editing = !this.editing;
    if(!this.editing) {
      this.edit.emit(this.detail);
    }
  }

  updateName(newName){
    this.detail.name = newName;
    //if(!this.editing){
      //this.detail = {...this.detail}
   // }
  }

  removePassenger(){
    this.remove.emit(this.detail);
  }
}
