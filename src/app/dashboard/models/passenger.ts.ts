export interface Passenger {
  id: number;
  name: string;
  age: number;
  checkedIn: boolean;
  checkedInDate: number | null;
  baggage: Luggage;
}
 export interface Kid {
  name: string;
  age: number
}
export interface Luggage {
  key: string;
  value: string;
}
