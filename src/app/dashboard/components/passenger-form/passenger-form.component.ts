import {Component, Input, OnInit} from '@angular/core';
import {Passenger} from "../../models/passenger.ts";

@Component({
  selector: 'app-passenger-form',
  templateUrl: './passenger-form.component.html',
  styleUrls: ['./passenger-form.component.css']
})
export class PassengerFormComponent implements OnInit {
  @Input() detail: Passenger;

  constructor() { }

  ngOnInit() {
  }

}
