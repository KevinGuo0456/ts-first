import { Component, Input, OnInit, OnChanges} from '@angular/core';
import {Passenger} from "../../models/passenger.ts";

@Component({
  selector: 'app-count',
  templateUrl: './count.component.html',
  styleUrls: ['./count.component.css']
})
export class CountComponent implements OnInit, OnChanges{

  checkedInNumber: number;
  @Input() items: Passenger[];
  constructor() {
    this.checkedInNumber = 0;
  }



  ngOnInit() {
    //let names = this.items.reduce((names,item)=> names += item.name + '| ',  'All Passengers names:');
    //console.log(names);
    //let a = [1,2,3,4,5];
    //let a2 = a.map((item)=> item = item * 2);
    //let a3 = this.items.filter(item => item % 2 === 0 );
    //let checkedInPassengers1 = this.items.map(item => 1);
    //console.log(checkedInPassengers1)
    //let checkedInPassengers2 = this.items.filter(item => !item.checkedIn);
    //console.log(checkedInPassengers2)
  }
  ngOnChanges(){//this is a lifecycle hook
    if(this.items)
    this.checkedInNumber = this.items.reduce((num,item)=> num += item.checkedIn ? 1 : 0 , 0 );

    /*let obj = {age: 30, name: 'Jane'};
    //let newObj = Object.assign ({}, obj );
    let newObj = {...obj};
    newObj.age = 22;
    console.log("test obj reference");
    console.log(obj.age);


    //primitive data
    let a = 30;
    let b = a;
    b = 25;
    console.log(a);*/

  }



}
