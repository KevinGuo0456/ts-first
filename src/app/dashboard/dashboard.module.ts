import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashComponent } from './containers/dash/dash.component';
import { CountComponent } from './components/count/count.component';
import { DetailComponent } from "./containers/detail/detail.component";
import {HttpClientModule} from "@angular/common/http";
import { DashboardViewerComponent } from './containers/dashboard-viewer/dashboard-viewer.component';
import { PassengerFormComponent } from './components/passenger-form/passenger-form.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  declarations: [DashComponent, DetailComponent, CountComponent, DashboardViewerComponent, PassengerFormComponent],
  exports: [DashComponent],
})

export class DashboardModule { }
